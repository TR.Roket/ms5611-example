/*"
 * MS5611.h
 *
 *  Created on: May 15, 2020
 *      Author: Tunahan Özkezer
 */
#include "main.h"
#include "math.h"
#ifndef INC_MS5611_H_
#define INC_MS5611_H_



#endif /* INC_MS5611_H_ */


#define CMD_MS5611_RESET 0x1E
#define CMD_MS5611_PROM_Setup 0xA0
#define CMD_MS5611_PROM_C1 0xA2
#define CMD_MS5611_PROM_C2 0xA4
#define CMD_MS5611_PROM_C3 0xA6
#define CMD_MS5611_PROM_C4 0xA8
#define CMD_MS5611_PROM_C5 0xAA
#define CMD_MS5611_PROM_C6 0xAC
#define CMD_MS5611_PROM_CRC 0xAE
#define CMD_CONVERT_D1_OSR4096 0x48   // Maximum resolution (oversampling)
#define CMD_CONVERT_D2_OSR4096 0x58   // Maximum resolution (oversampling)




void MS5611_init(SPI_HandleTypeDef *accSPI, GPIO_TypeDef *cs_GPIO_PORT, uint16_t cs_GPIO_Pin);
int16_t MS5611_Read(uint8_t bits);
uint16_t MS5611_readProm(uint8_t reg) ;
void MS5611_Reset(void);
void MS5611_Convert(const uint8_t addr, uint8_t bits);
uint32_t MS5611_readADC(void);
void MS5611_SPI_Write(uint8_t *writeData, uint16_t size);
uint8_t MS5611_SPI_Read(uint8_t readData);
uint16_t MS5611_Spi_Read_16bits(uint8_t reg);
int32_t  MS5611_getTemperature(void);
uint32_t MS5611_getPressure(void);
int16_t  MS5611_getLastResult(void);
uint16_t MS5611_getPromValue(uint8_t p);
float MS5611_get_altitude(void);
