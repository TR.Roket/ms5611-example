/*
 * MS5611.c
 *
 *  Created on: May 15, 2020
 *      Author: Tunahan Özkezer
 */

#include "MS5611.h"

SPI_HandleTypeDef *spiI;
GPIO_TypeDef *csPin_PORT;
uint16_t csPin_Pin;
int32_t  _temperature;
uint32_t _pressure,D1,D2,C[8];

void MS5611_init(SPI_HandleTypeDef *accSPI, GPIO_TypeDef *cs_GPIO_PORT, uint16_t cs_GPIO_Pin)
{
		spiI = accSPI;	/**/	csPin_PORT=cs_GPIO_PORT;	/**/	csPin_Pin=cs_GPIO_Pin;
	
	MS5611_Reset();

	  C[0] = 1;
	  C[1] = 40127;
	  C[2] = 36924;
	  C[3] = 23317;
	  C[4] = 23282;
	  C[5] = 33464;
	  C[6] = 28312;
	  C[7] = 0xF0F0;

	  for (uint8_t reg = 0; reg < 8; reg++)
	  {
	    C[reg] = MS5611_readProm(reg);
	  }

}

int16_t MS5611_Read(uint8_t bits)
{
  // VARIABLES NAMES BASED ON DATASHEET  <- Nice!
	MS5611_Convert(0x40, bits);
	uint32_t D1 = MS5611_readADC();

	MS5611_Convert(0x50, bits);
	uint32_t D2 = MS5611_readADC();

  uint32_t Tref, dT;
  uint32_t dTC6;
  int32_t  TEMP;
  Tref = C[5] * 256UL;
  if (D2 < Tref ) {
    dT   = Tref - D2;
    dTC6  = ((uint64_t)dT * (uint64_t)C[6]) >> 23;
    TEMP = 2000 - dTC6;
  } else {
    dT   = D2 - Tref;
    dTC6  = ((uint64_t)dT * (uint64_t)C[6]) >> 23;
    TEMP = 2000 + dTC6;
  }

  uint64_t offT1  =  (uint64_t)C[2] << 16;
  uint64_t TCOdT  = ((uint64_t)C[4] * (uint64_t)dT) >> 7;
  int64_t  OFF;
  if (D2 < Tref ) {
    OFF = offT1 - TCOdT;
  } else {
    OFF = offT1 + TCOdT;
  }

  uint64_t sensT1 =  (uint64_t)C[1] << 15;
  uint64_t TCSdT  = ((uint64_t)C[3] * (uint64_t)dT) >> 8;
  int64_t  SENS;
  if (D2 < Tref ) {
    SENS   = sensT1 - TCSdT;
  } else {
    SENS   = sensT1 + TCSdT;
  }

  uint32_t T2    = 0;
  uint32_t OFF2  = 0;
  uint32_t SENS2 = 0;
  if (TEMP < 2000)
  {
    uint64_t tSQ;
    T2     = ((uint64_t)dT * (uint64_t)dT) >> 31;
    tSQ    = (int32_t)TEMP - 2000L;
    tSQ   *= tSQ;
    OFF2   = (5ULL * (uint64_t)tSQ) >> 1;
    SENS2  = (5ULL * (uint64_t)tSQ) >> 2;

    if (TEMP < -1500)
    {
      tSQ    = (int32_t)TEMP - 2000L;
      tSQ   *= tSQ;
      OFF2  +=   7ULL * (uint64_t)tSQ;
      SENS2 += (11ULL * (uint64_t)tSQ) >> 1;
    }
  }

  TEMP -= T2;
  OFF  -= OFF2;
  SENS -= SENS2;

  int64_t P  = (int64_t)D1 * SENS;
  P /= 2097152LL;
  P -= OFF;
  P /= 32768LL;

  _temperature = TEMP;
  _pressure = (uint32_t)P;

  return 0;
}

void MS5611_Reset(void)
{
	uint8_t buffer;
	buffer=CMD_MS5611_RESET;
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiI, &buffer, 1, 10);
	HAL_Delay(4);
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);

}

void MS5611_Convert(const uint8_t addr, uint8_t bits)
{
  uint8_t del[5] = {1, 2, 3, 5, 10};
  uint8_t offset = (bits - 8) * 2;
  uint8_t buffer;

  buffer = addr + offset;

	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiI, &buffer, 1, 10);
	HAL_Delay(del[offset/2]);
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);
}

uint16_t MS5611_readProm(uint8_t reg) {
  // read two bytes from SPI and return accumulated value

  uint8_t offset = reg * 2;
  uint8_t buffer[2];
  uint8_t val[3];
  uint16_t return_data;

	buffer[0] = 0xA0 + offset;
	buffer[1]=0x00;

	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiI, buffer, 2, 10);
	HAL_SPI_Receive(spiI, val, 2, 10);
	return_data= ((uint16_t)val[0]<<8) | (val[1]);              // read 8 bits of data (LSB)
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);
  return return_data;
}

uint32_t MS5611_readADC(void) {
  // read three bytes from SPI and return accumulated value
  uint32_t val;
  uint8_t buffer=0;
  uint8_t recieving_data[3];
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiI, &buffer, 1, 10);
	HAL_SPI_Receive(spiI, recieving_data, 3, 10);

	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);
	val = (((uint32_t)recieving_data[0])<<16) | (((uint32_t)recieving_data[1])<<8) | (recieving_data[2]);


  return val;
}

void MS5611_SPI_Write(uint8_t *writeData, uint16_t size)
{
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiI, writeData, size, 10);
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);
}

uint8_t MS5611_SPI_Read(uint8_t readData)
{

	uint8_t recieving_data;
	uint8_t transmit_data = readData;
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiI, &transmit_data, 1, 10);
	HAL_SPI_Receive(spiI, &recieving_data, 1, 10);
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);

	return recieving_data;
}

uint16_t MS5611_Spi_Read_16bits(uint8_t reg) {
  // read two bytes from SPI and return accumulated value
	uint8_t  recieve_data[2];
	uint16_t return_value;
	uint8_t addr = reg; // | 0x80; // Set most significant bit
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiI, &addr, 1, 10);
	HAL_SPI_Receive(spiI, recieve_data, 2, 10);
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);
	return_value = ((uint16_t)recieve_data[0]<<8) | (recieve_data[1]);
	return return_value;
}

float MS5611_get_altitude(void)
{
	float tmp_float;
	float Altitude;

	tmp_float = ((float)_pressure / 101325.0);
	tmp_float = pow(tmp_float, 0.190295);
	Altitude = 44330.0 * (1.0 - tmp_float);

	return (Altitude);
}

 int32_t  MS5611_getTemperature()         { return _temperature; };
 uint32_t MS5611_getPressure()            { return _pressure; };
 uint16_t MS5611_getPromValue(uint8_t p)  { return C[p]; };
